<?php

namespace yii2portal\feedback\controllers;

use Yii;
use yii2portal\core\controllers\Controller;
use yii2portal\feedback\models\Contact;

/**
 * Site controller
 */
class IndexController extends Controller
{

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }


    public function actionIndex($structure_id)
    {

        $model = new Contact();
        $model->withCaptcha();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->refresh();
        }
        $page = Yii::$app->modules['structure']->getPage($structure_id);
        return $this->render('index', [
            'page' => $page,
            'model' => $model,
            'captchaAction' => "{$page->urlPath}captcha"
        ]);
    }

}
