<?php

namespace yii2portal\feedback\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property integer $id
 * @property string $name
 * @property string $contact
 * @property string $message
 * @property string $ip
 * @property integer $dateline
 * @property integer $status
 */
class Contact extends \yii\db\ActiveRecord
{
    public $captcha;
    protected $_isWithCaptcha;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = [
            [['message'], 'string'],
            [['message', 'name', 'contact'], 'required'],
            [['dateline', 'status'], 'integer'],
            [['name', 'contact'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 50],
        ];

        if($this->_isWithCaptcha){
            $rules[] = ['captcha', 'required'];
            $rules[] = ['captcha', 'captcha'];
        }

        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'contact' => 'Контакты',
            'message' => 'Сообщение',
            'ip' => 'Ip адрес',
            'dateline' => 'Дата отправки',
            'status' => 'Status',
            'captcha' => 'Каптча',
        ];
    }

    /**
     * @inheritdoc
     * @return ContactQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ContactQuery(get_called_class());
    }

    public function withCaptcha($is = true){
        $this->_isWithCaptcha = $is;
    }
}
