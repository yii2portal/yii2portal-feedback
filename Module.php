<?php

namespace yii2portal\feedback;

use Yii;

class Module extends \yii2portal\core\Module
{

    public $controllerNamespace = 'yii2portal\feedback\controllers';

    const EVENT_INSERT_FEEDBACK = 'insertFeedback';


}